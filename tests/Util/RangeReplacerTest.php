<?php

namespace App\Tests\Util;

use App\Util\RangeReplacer;
use PHPUnit\Framework\TestCase;

class RangeReplacerTest extends TestCase
{
    /**
     * @var $rangeReplacer RangeReplacer
     */
    private $rangeReplacer;

    /**
     * @before
     */
    public function setUp()
    {
        $this->rangeReplacer = new RangeReplacer();
    }

    /**
     * @after
     */
    public function tearDown()
    {
        $this->rangeReplacer = null;
    }

    public function testIfMultipleOf2AreReplace()
    {
        $this->assertEquals('DEUX', $this->rangeReplacer->replace(2));
        $this->assertEquals('DEUX', $this->rangeReplacer->replace(4));
    }

    public function testIfMultipleOf8AreReplace()
    {
        $this->assertEquals('HUIT', $this->rangeReplacer->replace(8));
        $this->assertEquals('HUIT', $this->rangeReplacer->replace(24));
    }

    public function testIfMultipleOf16AreReplace()
    {
        $this->assertEquals('SEIZE', $this->rangeReplacer->replace(16));
        $this->assertEquals('SEIZE', $this->rangeReplacer->replace(48));
    }

    public function testIfMultipleOf32AreReplace()
    {
        $this->assertEquals('OCTET', $this->rangeReplacer->replace(32));
        $this->assertEquals('OCTET', $this->rangeReplacer->replace(64));
    }

    public function testReplaceInterval()
    {
        $this->assertEquals('1DEUX', $this->rangeReplacer->replaceInterval(1,2));
        $this->assertEquals('1DEUX3DEUX5DEUX7HUIT', $this->rangeReplacer->replaceInterval(1,8));
    }

    public function testException()
    {
        $this->expectException(\TypeError::class);
        $this->rangeReplacer->replaceInterval('A','B');
    }
}
