<?php
namespace App\Util;


class RangeReplacer
{

    /**
     * @param int $min
     * @param int $max
     * @return string
     */
    public function replaceInterval(int $min, int $max)
    {
        $result = "";
        while ($min <= $max) {
            $result .= $this->replace($min);
            $min++;
        }
        return $result;
    }

    public function replace($number)
    {
        if ($number % 32 == 0) {
            return 'OCTET';
        }
        if ($number % 16 == 0) {
            return 'SEIZE';
        }
        if ($number % 8 == 0) {
            return 'HUIT';
        }
        if ($number % 2 == 0) {
            return 'DEUX';
        }
        return $number;
    }
}